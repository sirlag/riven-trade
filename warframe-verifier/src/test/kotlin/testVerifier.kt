import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import trade.rivenmods.WarframeVerifier.verificationHash
import trade.rivenmods.WarframeVerifier.verifyHash

class TestVerifier : Spek({
    describe("a hash test"){
        var hash: Int? = null

        it("should make a consistent hash"){
            hash = verificationHash("fake|85465456",
                    "https://forums.warframe.com/profile/757416-sirlag?tab=field_core_pfield_1")
            assert(hash!=null && hash == 1490520237)
        }

        it("Should find and confirm the hash"){
            val username = verifyHash("fake|85465456",
                    "https://forums.warframe.com/profile/757416-sirlag?tab=field_core_pfield_1", hash!!, "rtv")
            assert(username.isPresent)
        }
    }
})