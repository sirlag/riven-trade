import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './Main.vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import VueClipboards from 'vue-clipboards'

import AuthService from './Auth/AuthService.js'

import Home from './Home.vue'
import Callback from './Auth/Callback.vue'
import Profile from './Profile/ProfilePage.vue'
import Search from './Search/search.vue'
import Faq from './About/faq.vue'
import RivenPage from './Riven/Rivens.vue'

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VueClipboards);

Vue.component('search', Search)
Vue.component('profile-menu', require('./Menu/profile-menu.vue'))
Vue.component('wf-icon', require('./wf-icons/WFIcon.vue'))
Vue.component('wf-riven', require('./Riven/RivenListing.vue'))


const routes = [
    {path: '/home', name: 'Home', component: Home},
    {path: '/faq', name: 'FAQ', component: Faq},
    {path: '/callback', name: 'Callback', component: Callback},
    {path: '/profile', name: 'Profile', component: Profile},
    {path: '/rivens', name: 'Rivens', component: RivenPage},
    {path: '*', redirect: '/home'}
]

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})

export default router
