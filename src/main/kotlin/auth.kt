import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.gson.Gson
import com.nimbusds.jose.util.X509CertUtils
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.net.URL
import java.security.interfaces.RSAPublicKey
import java.time.Instant
import java.time.LocalDateTime
import java.util.*


private var managementJWT: DecodedJWT? = null

data class tokenConfig( val client_id: String = "tdKPPYZzodHKYcRDKJz37hUbiCZF5hP9",
                        val client_secret: String = System.getenv("mgmtCSecret"),
                        val audience: String = "https://sirlag-riven.auth0.com/api/v2/",
                        val grant_type: String = "client_credentials")

fun getManagementJWT(): DecodedJWT? {


    if(managementJWT == null || Date.from(Instant.now()).after(managementJWT!!.expiresAt)){
        println("Fetching new access_token at ${LocalDateTime.now()}")
        val gson = Gson()
        val token = tokenConfig()

        val (_, _, result) = "https://sirlag-riven.auth0.com/oauth/token".httpPost().body(gson.toJson(token))
                .header(Pair("content-type", "application/json")).responseString()
        result.success {
                val managementResponse = gson.fromJson(result.get(), managementJWTResponse::class.java)
                managementJWT = JWT.decode(managementResponse.access_token)
                println("New management access_token will expire at ${managementJWT?.expiresAt}")
            }
        result.failure { println(result.component2()) }
    }
    return managementJWT
}


fun verifyJWT(header: String): Boolean{
    val token = header.removePrefix("Bearer ")
    try {
        val parsedJWT = JWT.decode(token)
        val alg = Algorithm.RSA256(getPublicRSAKey(parsedJWT.keyId), null)
        val verifier = JWT.require(alg)
                .withIssuer("https://sirlag-riven.auth0.com/")
                .withAudience("https://rivenmods.trade")
                .build()
        val jwt = verifier.verify(token)
        return true
    } catch (ex: UnsupportedEncodingException){
        //UTF-8 encoding not supported
        println("UNABLE TO READ IT")
    } catch (ex: JWTVerificationException){
        //Invalid signature/claims
        println("ERROR VERIFYING TOKEN ${ex.localizedMessage}")
    } catch (ex: Exception){
        println(ex)
    }
    return false
}

fun parsedJWT(header: String): DecodedJWT?{
    val token = header.removePrefix("Bearer ")
    try {
        val parsedJWT = JWT.decode(token)
        val alg = Algorithm.RSA256(getPublicRSAKey(parsedJWT.keyId), null)
        val verifier = JWT.require(alg)
                .withIssuer("https://sirlag-riven.auth0.com/")
                .withAudience("https://rivenmods.trade")
                .build()
        return verifier.verify(token)

    } catch (ex: UnsupportedEncodingException){
        //UTF-8 encoding not supported
        println("UNABLE TO READ IT")
    } catch (ex: JWTVerificationException){
        //Invalid signature/claims
        println("ERROR VERIFYING TOKEN ${ex.localizedMessage}")
    } catch (ex: Exception){
        println(ex)
    }
    return null
}

fun fetchJWKS(): JWKS?{
    val gson = Gson()
    try{
        val input = URL("https://sirlag-riven.auth0.com/.well-known/jwks.json").openStream()
        val reader = InputStreamReader(input, "UTF-8")
        val jwks = gson.fromJson<JWKS>(reader, JWKS::class.java)
        return jwks
    } catch (ignored: Exception){
        println("ERROR: didn't work :/ : $ignored")
    }
    return null
}


data class JWKS(val keys:List<JWK>)

fun makeRSAPublicKey(key: String): RSAPublicKey{
    val cert = X509CertUtils.parse("-----BEGIN CERTIFICATE-----\n$key\n-----END CERTIFICATE-----")
    if(cert == null)
        println("Unable to Parse Cert")
    return cert.publicKey as RSAPublicKey
}

fun getPublicRSAKey(kid: String): RSAPublicKey{
    return makeRSAPublicKey(JwkStore?.keys?.filter { it.kid == kid }?.first()?.x5c?.first()!!)
}

data class JWK(val alg: String, val kty: String, val use: String, val x5c: List<String>, val n: String, val e: String,
               val kid: String, val x5t: String)
data class managementJWTResponse(val access_token: String, val expires_in: Int, val scope: String, val token_type:
String)