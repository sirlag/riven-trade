import com.google.gson.Gson
import com.mongodb.MongoClient
import com.mongodb.MongoWriteException
import org.bson.Document
import org.bson.types.ObjectId
import java.util.*

val mongoClient = MongoClient("192.168.254.19", 32768)
val rivenDatabase = mongoClient.getDatabase("riventrade")


fun goOnline(user: String){
    val collection = rivenDatabase.getCollection("users")
    val doc = Document("online", user)
    try{
        collection.insertOne(doc)
    }catch(ex: MongoWriteException){

    }
}

fun goOffline(user: String){
    val collection = rivenDatabase.getCollection("users")
    val doc = Document("online", user)
    collection.deleteOne(doc)
}

fun isOnline(user: String): Boolean{
    val collection = rivenDatabase.getCollection("users")
    val doc = Document("online", user)
    val result = collection.find(doc).firstOrNull()
    if(result != null)
        return true
    return  false
}

fun ListRiven(riven: Riven, rivenUser: RivenUser, sub: String){
    val gson = Gson()
    val collection = rivenDatabase.getCollection("rivens")
    val doc = Document("riven", Document.parse(gson.toJson(riven)))
    val userDoc = Document("wfName", rivenUser.wfname)
            .append("userClass", rivenUser.userClass.toString())
            .append("sub", sub)
    doc.append("user", userDoc)
    doc.append("insertDate", Date())
    collection.insertOne(doc)
}

fun recentRiven(limit: Int): String{
    val collection = rivenDatabase.getCollection("rivens")

    val results = collection.find().sort(Document("insertDate", -1)).limit(limit)
    return results.map{it.toJson()}.joinToString(", ", "[", "]")
}

fun DeleteRiven(id: String){
    val collection = rivenDatabase.getCollection("rivens")
    val obid = ObjectId(id)
    collection.deleteOne(Document("_id", obid))
}

fun SearchRivens(riven: Riven?, rivenUser: RivenUser?, sub: String?): String{
    val gson = Gson()
    val collection = rivenDatabase.getCollection("rivens")

   // val testDoc = Document("riven.weapon", "Broken War")
   //         .append("riven.type", "Melee")
   // println(collection.find(testDoc).firstOrNull()?.toString())

    val query = Document()

    if(riven != null){
        if(riven.weapon != null)
            query.append("riven.weapon", riven.weapon)
        if(riven.type != null)
            query.append("riven.type", riven.type.toString().toLowerCase().capitalize())
        if(riven.name != null && riven.name.isNotEmpty())
            query.append("riven.name", riven.name)
        if(riven.masteryRank > 8)
            query.append("riven.masterRank", riven.masteryRank)
        if(riven.price > 0)
            query.append("riven.price", Document("\$lt", riven.price))
        if(riven.cycles > 0)
            query.append("riven.cycles", Document("\$lt", riven.cycles))
        if(riven.polarity != null)
            query.append("riven.polarity", riven.polarity.toString().toLowerCase().capitalize())
        if(riven.mods != null && riven.mods.isNotEmpty()){
            query.append("riven.mods", Document("\$all", riven.mods.filter { it.property != null }.map { it.property }
                    .toList()))
        }

    }

    if(sub != null && sub.isNotEmpty())
        query.append("user.sub", sub)

    println(query)
    val results = collection.find(query).limit(50).sort(Document("insertDate", -1))
    val totalResults = collection.count(query)
    val resultsJSON = results.map{it.toJson()}.joinToString(", ", "[", "]")

    return """
        {
            "results": $resultsJSON,
            "totalResults": $totalResults
        }
    """.trimIndent()

}

