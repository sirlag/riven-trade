import com.auth0.client.mgmt.ManagementAPI
import com.auth0.exception.APIException
import com.auth0.exception.Auth0Exception
import com.auth0.json.mgmt.users.User
import com.google.gson.Gson
import spark.Spark.*
import spark.Filter
import spark.route.HttpMethod
import trade.rivenmods.WarframeVerifier.verificationHash
import trade.rivenmods.WarframeVerifier.verifyHash



val JwkStore = fetchJWKS()


fun main(args: Array<String>){

    enableCORS("http://localhost:8080", "","")
    val gson = Gson()

    before("/api/*"){ request, response ->
        if(HttpMethod.post.name.equals(request.requestMethod(), true)) {
            val header = request.headers("Authorization")
            if(header != null){
                verifyJWT(header)
            } else {
                halt(401, "No Authorization header found")
            }
        }
    }
    get("/api/testEndpoint"){request, response ->
        if(request.headers("Authorization") != null)
            if(!verifyJWT(request.headers("Authorization")))
                halt(401, "Authorization failed")
        else
            halt(401, "No Authorization header found")
        "Hello world"
    }


    post("/api/listRiven"){request, response ->
        try{
            val jwt = parsedJWT(request.headers("Authorization"))
            val rivenRequest = gson.fromJson(request.body(), RivenRequest::class.java)

            //Check riven data
            val rivenMod = rivenRequest.rivenMod
            rivenMod.javaClass.fields
                    .filter { it == null }
                    .forEach { halt(400, "Missing part of riven data") }
            if(rivenMod.mods.size !in 2..4)
                halt(400, "Listing a riven mod requires between 2 and 4 properties")
            if(rivenMod.masteryRank !in 8..16)
                halt(400, "The listed riven mod must be between mastery rank 8 and 16")
            if(rivenMod.price < 1)
                halt(400, "You can not list a mod for less than 1 platinum")
            if(rivenMod.cycles < 0)
                halt(400, "You can not have a negative number of cycles on a riven mod.")
            if(rivenMod.rank < 0 || rivenMod.rank > 8)
                halt(400, "The listed riven mod must be between rank 0 and 8")

            println(rivenRequest.rivenUser)
            ListRiven(rivenMod, rivenRequest.rivenUser, jwt!!.subject)
            response.status(200)
        } catch (ex: Exception){
            ex.printStackTrace()
        }
    }

    delete("/api/listRiven"){request, _ ->
        val deleteID = request.queryParams("id")
        if(deleteID != null){
            DeleteRiven(deleteID)
        }
    }

    get("/publicApi/rivens"){request, response ->
        val limit = request.queryParams("limit").toInt()
        recentRiven(limit)
    }

    post("/publicApi/rivens"){ request, response ->
        val json = request.body()
        val rivenRequest = gson.fromJson(json, RivenRequest::class.java)
        val res = SearchRivens(rivenRequest.rivenMod, rivenRequest.rivenUser, rivenRequest.sub)
        println(res)
        res
    }

    post("/api/getHash"){request, response ->
        val profileUrl = normalizeWarframeURL(request.body())
        val jwt = parsedJWT(request.headers("Authorization"))
        val hash = verificationHash(jwt!!.subject, profileUrl)
        gson.toJson(hash)
    }

    post("/api/verifyWarframeAccount"){request, response ->
        val resBody = gson.fromJson(request.body(), vwaBody::class.java)
        val jwt = parsedJWT(request.headers("Authorization"))
        val user = verifyHash(jwt!!.subject, normalizeWarframeURL(resBody.profLink), resBody.hashCode, "rtv")
        if(user.isPresent){
            val wfUser = user.get()
            val managementToken = getManagementJWT()

            val mgmt = ManagementAPI("sirlag-riven.auth0.com", managementToken?.token)
            val appMetaUser = User()
            val appMetadata = HashMap<String, Any>()
            appMetadata["wfverified"] = true
            appMetadata["wfname"] = wfUser
            appMetaUser.appMetadata = appMetadata
            val mgmtRequest = mgmt.users().update(jwt.subject, appMetaUser)

            try{
                mgmtRequest.execute()
            } catch (ex: APIException) {
                // api error
            } catch (ex: Auth0Exception) {
                // request error
            }

        }
        else
            halt(400, "Error verifying account")
        gson.toJson(user.get())
    }

    post("/api/goOnline"){request, _ ->
        goOnline(request.body())
    }

    post("/api/goOffline"){request, _ ->
        goOffline(request.body())
    }

    get("/publicApi/online"){request, response ->
        val query = request.queryParams("name")
        gson.toJson(isOnline(query))
    }

    get("/testAPI/testToken"){request, response ->
        getManagementJWT()
        "hello!"
    }
}

//Cors
private fun enableCORS(origin: String, methods: String, headers: String) {

    options("/*") { request, response ->

        val accessControlRequestHeaders = request.headers("Access-Control-Request-Headers")
        if (accessControlRequestHeaders != null) {
            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders)
        }

        val accessControlRequestMethod = request.headers("Access-Control-Request-Method")
        if (accessControlRequestMethod != null) {
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod)
        }

        "OK"
    }

    before(Filter{ request, response ->
        response.header("Access-Control-Allow-Origin", origin)
        response.header("Access-Control-Request-Method", methods)
        response.header("Access-Control-Allow-Headers", headers)
        // Note: this may or may not be necessary in your particular application
        response.type("application/json")
    })
}
