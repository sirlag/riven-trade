import com.google.gson.annotations.SerializedName

data class Riven(val weapon:String, val type: RivenType, val polarity: Polarity, val masteryRank: Int,
                 val mods: List<RivenEffect>, val cycles: Int, val price: Int, val rank: Int, val name: String)
data class RivenEffect(val value:Double, val property: String)
enum class RivenType{
    @SerializedName("Primary")
    PRIMARY,
    @SerializedName("Shotgun")
    SHOTGUN,
    @SerializedName("Pistol")
    PISTOL,
    @SerializedName("Melee")
    MELEE
}
enum class Polarity{
    @SerializedName("Madurai")
    MADURAI,
    @SerializedName("Vazarin")
    VAZARIN,
    @SerializedName ("Naramon")
    NARAMON
}

//User Class
enum class UserClass{
    @SerializedName("Prime") PRIME,
    @SerializedName("Regular") REGULAR
}

//Request Data Classes
data class vwaBody(val hashCode: Int, val profLink: String)
data class RivenUser(val userClass: UserClass, val wfname: String)
data class RivenRequest(val rivenMod: Riven, val rivenUser: RivenUser, val sub: String)


//Util funs
fun normalizeWarframeURL(url: String): String{
    val queryString = "?tab=field_core_pfield_1"
    return if(url.endsWith(queryString)) url else url + queryString
}
